﻿namespace Submissions.Common.Core
{
    public static class ApplicationStates
    {
        public const string Background = "Background";

        public const string Foreground = "Foreground";
    }
}