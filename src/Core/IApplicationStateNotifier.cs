﻿using System;

namespace Submissions.Common.Core
{
    public interface IApplicationStateNotifier
    {
        IObservable<string> ApplicationStateChanges { get; }
    }
}