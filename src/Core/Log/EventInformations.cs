﻿using System;
using System.Diagnostics.Tracing;

namespace Submissions.Common.Core.Log
{
    public struct EventInformations : IEquatable<EventInformations>
    {
        public DateTime TimeStamp { get; set; }

        public string Message { get; set; }

        public EventLevel Level { get; set; }

        public int EventId { get; set; }

        public bool Equals(EventInformations other)
        {
            return TimeStamp.Equals(other.TimeStamp) && string.Equals(Message, other.Message) && Level == other.Level && EventId == other.EventId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            return obj is EventInformations informations && Equals(informations);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = TimeStamp.GetHashCode();
                hashCode = (hashCode * 397) ^ (Message != null ? Message.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int)Level;
                hashCode = (hashCode * 397) ^ EventId;
                return hashCode;
            }
        }

        public static bool operator ==(EventInformations left, EventInformations right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(EventInformations left, EventInformations right)
        {
            return !left.Equals(right);
        }
    }
}