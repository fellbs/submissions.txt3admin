﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Submissions.Common.Core.Log
{
    public interface IInMemoryLogEventListener
    {
        bool HasErrors { get; }

        List<EventInformations> GetLog();

        List<EventInformations> GetLogError();

        IObservable<EventInformations> EventAdded { get; }
    }
}
