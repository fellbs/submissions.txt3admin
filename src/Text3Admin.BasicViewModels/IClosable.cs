﻿using System.Threading.Tasks;

namespace Submissions.Text3Admin.BasicViewModels
{
    public interface IClosable
    {
        Task CloseAsync();
    }
}