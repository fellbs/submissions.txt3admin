﻿using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using MvvmCross.IoC;
using MvvmCross.Navigation;
using Submissions.Schedul;

namespace Submissions.Text3Admin.BasicViewModels
{
    public abstract class PageViewModel : MvxViewModel, IClosable, IDisposable
    {
        private bool _disposed;

        [MvxInject]
        public IMvxNavigationService MvxNavigationService { get; set; }

        public override void ViewAppearing()
        {
            _disposed = false;
        }

        public async Task CloseAsync()
        {
            await MvxNavigationService.Close(this).ContinueOnCurrentContext();
            Dispose();
        }

        protected virtual void OnDisposing()
        {
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                OnDisposing();
                _disposed = true;
            }
        }
    }
}
