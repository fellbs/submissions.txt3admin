﻿using MvvmCross;
using MvvmCross.ViewModels;
using Submissions.DependencyInjection;
using Submissions.DependencyInjection.MvvmCross;


namespace Submissions.Text3Admin.Core
{
    public abstract class Text3AdminApp : MvxApplication
    {
        public override void Initialize()
        {
            var dependencyInjectionContainer = new MvxDependencyInjectionContainerAdapter();
            RegisterSystemBoundaries(dependencyInjectionContainer);
            InitDependencyInjection(dependencyInjectionContainer);
            //RegisterAppStart<SetupViewModel>();
        }

        protected abstract void RegisterSystemBoundaries(IDependencyInjectionContainer dependencyInjectionContainer);

        protected virtual void InitDependencyInjection(
            MvxDependencyInjectionContainerAdapter dependencyInjectionContainer)
        {
            Mvx.IoCProvider.RegisterSingleton<IDependencyInjectionContainer>(dependencyInjectionContainer);
            Mvx.IoCProvider.RegisterSingleton<IDependencyInjectionConstructor>(dependencyInjectionContainer);
            Mvx.IoCProvider.RegisterSingleton<IDependencyInjectionResolver>(dependencyInjectionContainer);
        }
    }
}
