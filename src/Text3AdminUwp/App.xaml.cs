﻿using System;
using System.Globalization;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml.Navigation;
using DevExpress.UI.Xaml.Layout;
using MvvmCross;
using Submissions.Common.Core;
using Submissions.Common.Windows.Logging;
using Submissions.Text3Admin.Windows.Exceptions;

namespace Submissions.Text3Admin.Windows
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : IApplicationStateNotifier, IDisposable
    {
        private readonly Subject<string> _stateChanges = new Subject<string>();

        public IObservable<string> ApplicationStateChanges => _stateChanges;

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            InitializeComponent();

            var logger = Log.Instance;
            GlobalExceptionHandler.SetGlobalExceptionHandlers(logger);
            Suspending += OnSuspending;
            EnteredBackground += OnEnteredBackground;
            LeavingBackground += OnLeavingBackground;

            logger.Info("Application has initialized component");
            var currentCulture = CultureInfo.CurrentCulture;
            var currentUiCulture = CultureInfo.CurrentUICulture;
            logger.Info($"CurrentCulture: {currentCulture}, CurrentUICulture: {currentUiCulture}");
        }

        protected override void RunAppStart(IActivatedEventArgs activationArgs)
        {
            base.RunAppStart(activationArgs);
            Mvx.IoCProvider.RegisterSingleton<IApplicationStateNotifier>(this);
            Observable.FromAsync(ExtendedCapabilities.PreventSuspend)
                .Subscribe(_ => Log.Instance.Info("PREVENT SUSPEND ENABLED"), ex => Log.Instance.Error("PREVENT SUSPEND FAILED", ex));
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        protected override void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new NavigationException("Failed to load Page " + e.SourcePageType.FullName);
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used such as when the application is launched to open a specific file.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        //        protected override async void OnLaunched(LaunchActivatedEventArgs e)
        //        {
        //#if DEBUG
        //            if (System.Diagnostics.Debugger.IsAttached)
        //            {
        //                this.DebugSettings.EnableFrameRateCounter = true;
        //            }
        //#endif
        //            Frame rootFrame = Window.Current.Content as Frame;

        //            // Do not repeat app initialization when the Window already has content,
        //            // just ensure that the window is active
        //            if (rootFrame == null)
        //            {
        //                MainViewModel viewModel = new MainViewModel();

        //                // Create a Frame to act as the navigation context and navigate to the first page
        //                rootFrame = new HamburgerMenuFrame(typeof(HamburgerMenuPage)) { ViewModel = viewModel, DataContext = viewModel };
        //                Interaction.GetBehaviors(rootFrame).Add(new NavigationService());
        //                SuspensionManager.RegisterFrame(rootFrame, "AppFrame");
        //                rootFrame.NavigationFailed += OnNavigationFailed;

        //                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated || e.PreviousExecutionState == ApplicationExecutionState.ClosedByUser)
        //                {
        //                    //TODO: Load state from previously suspended application
        //                    await SuspensionManager.RestoreAsync();
        //                }

        //                // Place the frame in the current Window
        //                Window.Current.Content = rootFrame;
        //            }

        //            if (e.PrelaunchActivated == false)
        //            {
        //                if (rootFrame.Content == null)
        //                {
        //                    // When the navigation stack isn't restored navigate to the first page,
        //                    // configuring the new page by passing required information as a navigation
        //                    // parameter
        //                    rootFrame.Navigate(typeof(MainPage), e.Arguments);
        //                }
        //                // Ensure the current window is active
        //                Window.Current.Activate();
        //            }
        //        }
        
        protected override void OnEnteredBackground(object sender, EnteredBackgroundEventArgs args)
        {
            var deferral = args.GetDeferral();
            Log.Instance.Debug("Application is entering background");
            _stateChanges.OnNext(ApplicationStates.Background);
            deferral.Complete();
        }

        protected override void OnLeavingBackground(object sender, LeavingBackgroundEventArgs args)
        {
            var deferral = args.GetDeferral();
            Log.Instance.Debug("Application is leaving background");
            _stateChanges.OnNext(ApplicationStates.Foreground);
            deferral.Complete();
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        protected override void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            SuspensionManager.SaveAsync();
            Log.Instance.Debug("Application is suspending");
            deferral.Complete();
        }

        public void Dispose()
        {
            _stateChanges.Dispose();
        }
    }
}
