﻿using System;

namespace Submissions.Text3Admin.Windows.Exceptions
{
    public class NavigationException : Exception
    {
        public NavigationException(string message)
            : base(message)
        {
        }
    }
}