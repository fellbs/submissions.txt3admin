﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.ExtendedExecution.Foreground;

namespace Submissions.Text3Admin.Windows
{
    public static class ExtendedCapabilities
    {
        private static ExtendedExecutionForegroundSession _session;

        public static async Task PreventSuspend()
        {
            var newSession = new ExtendedExecutionForegroundSession { Reason = ExtendedExecutionForegroundReason.Unconstrained };
            newSession.Revoked += SessionRevoked;

            var result = await newSession.RequestExtensionAsync();
            switch (result)
            {
                case ExtendedExecutionForegroundResult.Allowed:
                    _session = newSession;
                    break;
                default:
                    newSession.Dispose();
                    break;
            }
        }

        private static void SessionRevoked(object sender, ExtendedExecutionForegroundRevokedEventArgs args)
        {
            if (_session != null)
            {
                _session.Dispose();
                _session = null;
            }
        }
    }
}