﻿using DevExpress.UI.Xaml.Layout;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Submissions.Text3Admin.Windows
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : DXPage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }
    }
}
