﻿using System.Collections.ObjectModel;
using System.Linq;
using DevExpress.Core;
using DevExpress.Mvvm;

namespace Submissions.Text3Admin.Windows {
    public class MainViewModel : NavigationViewModelBase, ISupportNavigationEvents {        
        public ObservableCollection<HamburgerMenuBottomBarItemViewModel> BottomBarHamburgerMenuItems { get; } = new ObservableCollection<HamburgerMenuBottomBarItemViewModel>();
        public ObservableCollection<HamburgerMenuItemViewModel> HamburgerMenuItems { get; } = new ObservableCollection<HamburgerMenuItemViewModel>();
        public HamburgerMenuItemViewModel SelectedHamburgerMenuItem {
            get { return GetProperty<HamburgerMenuItemViewModel>(); }
            set { SetProperty(value); }
        }
        bool IsStateRestored { get; set; }
        public MainViewModel() {
            InitializeHamburgerMenuItems();
            InitializeBottomBarHamburgerMenuItems();
        }

        private void InitializeBottomBarHamburgerMenuItems() {
            BottomBarHamburgerMenuItems.Add(new HamburgerMenuBottomBarItemViewModel("\xE713", "SettingsPage"));
        }

        private void InitializeHamburgerMenuItems() {
            HamburgerMenuItems.Add(new HamburgerMenuItemViewModel("MainPage", "\xE7C3", "MainPage"));
            HamburgerMenuItems.Add(new HamburgerMenuItemViewModel("Page with Pivot", "\xE7C3", "PageWithPivot"));
            HamburgerMenuItems.Add(new BottomHamburgerMenuItemViewModel("Page with Header", "\xE7C3", "PageWithHeader"));
        }

        public void OnNavigated(DXNavigationEventArgs e) {
            if(IsStateRestored) return;

            string selectedPage = null;
            if(e.Storage.TryGetParameter("SelectedPage", out selectedPage)) {
                SelectedHamburgerMenuItem = HamburgerMenuItems.FirstOrDefault(x => x.NavigationTargetTypeName == selectedPage);
            } else {
                SelectedHamburgerMenuItem = HamburgerMenuItems[0];
            }

            IsStateRestored = true;
        }

        public void OnNavigating(DXNavigatingCancelEventArgs e) {
            e.CurrentPageStorage.SaveParameter("SelectedPage", e.CurrentPageType);
        }
    }
    public class HamburgerMenuItemBaseViewModel {
        public string Icon { get; }
        public HamburgerMenuItemBaseViewModel(string icon) {
            Icon = icon;
        }
    }
    public class HamburgerMenuItemViewModel : HamburgerMenuItemBaseViewModel {
        public string Header { get; }
        public string NavigationTargetTypeName { get;  }
        public HamburgerMenuItemViewModel(string header, string icon, string navigationTargetTypeName) : base(icon) {
            Header = header;
            NavigationTargetTypeName = navigationTargetTypeName;
        }
    }
    public class BottomHamburgerMenuItemViewModel : HamburgerMenuItemViewModel {
        public BottomHamburgerMenuItemViewModel(string header, string icon, string navigationTargetTypeName) : base(header, icon, navigationTargetTypeName) {
        }
    }
    public class HamburgerMenuBottomBarItemViewModel : HamburgerMenuItemBaseViewModel {
        public string NavigationTargetTypeName { get; }
        public HamburgerMenuBottomBarItemViewModel(string icon, string navigationTargetTypeName) : base(icon) {
            NavigationTargetTypeName = navigationTargetTypeName;
        }
    }
}