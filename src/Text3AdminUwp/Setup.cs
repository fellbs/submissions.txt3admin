﻿using System.Diagnostics.Tracing;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.Platforms.Uap.Core;
using Submissions.Common.Windows.Logging;
using Submissions.Logging;
using Submissions.Text3Admin.Windows.Environment;

namespace Submissions.Text3Admin.Windows
{
    public class Setup : MvxWindowsSetup<Text3AdminWindowsApp>
    {
        protected override IMvxIocOptions CreateIocOptions()
            => new MvxIocOptions { PropertyInjectorOptions = new MvxPropertyInjectorOptions() { InjectIntoProperties = MvxPropertyInjection.MvxInjectInterfaceProperties } };

        protected override void InitializeFirstChance()
        {
            var log = Log.Instance;
            Mvx.IoCProvider.RegisterSingleton<ILog>(log);
            log.IsDebugEnabled = true;
            var inMemoryLogEventListener = new InMemoryLogEventListener();
            log.RegisterListener(inMemoryLogEventListener, EventLevel.LogAlways);
        }
    }
}