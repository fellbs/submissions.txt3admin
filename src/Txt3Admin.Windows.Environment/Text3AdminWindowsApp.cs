﻿using Submissions.DependencyInjection;
using Submissions.Text3Admin.Core;

namespace Submissions.Text3Admin.Windows.Environment
{
    public class Text3AdminWindowsApp : Text3AdminApp
    {
        protected override void RegisterSystemBoundaries(IDependencyInjectionContainer dependencyInjectionContainer)
        {
        }
    }
}
