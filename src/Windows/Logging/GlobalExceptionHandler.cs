﻿using System.Threading.Tasks;
using Windows.UI.Xaml;

namespace Submissions.Common.Windows.Logging
{
    public static class GlobalExceptionHandler
    {
        private static Log _log;

        public static void SetGlobalExceptionHandlers(Log log)
        {
            _log = log;

            if (Application.Current != null)
            {
                Application.Current.UnhandledException += HandleUnhandledException;
            }

            TaskScheduler.UnobservedTaskException += HandleUnobservedTaskException;
        }

        private static void HandleUnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            e.SetObserved();
            _log.Error(sender + ": " + e.Exception);
        }

        private static void HandleUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            _log.Error(sender + ": " + e.Exception);
        }
    }
}