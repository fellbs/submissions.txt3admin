﻿using System;
using System.Collections.Generic;
using Submissions.Common.Core.Log;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Reactive.Subjects;

namespace Submissions.Common.Windows.Logging
{
    public sealed class InMemoryLogEventListener : EventListener, IInMemoryLogEventListener
    {
        private readonly List<EventInformations> _events;
        private readonly Subject<EventInformations> _eventAdded = new Subject<EventInformations>();
        private readonly object _eventsLock = new object();

        public InMemoryLogEventListener()
        {
            _events = new List<EventInformations>();
            HasErrors = false;
        }

        protected override void OnEventWritten(EventWrittenEventArgs eventData)
        {
            lock (_eventsLock)
            {
                var informations = new EventInformations
                {
                    EventId = eventData.EventId,
                    TimeStamp = DateTime.Now,
                    Level = eventData.Level,
                    Message = FormatMessage(eventData),
                };

                if (IsErrorOrCritical(informations))
                {
                    HasErrors = true;
                }

                _events.Add(informations);
                _eventAdded.OnNext(informations);
            }
        }

        private static string FormatMessage(EventWrittenEventArgs eventData)
        {
            if (eventData == null)
            {
                return string.Empty;
            }

            var fullMessage = eventData?.Message != null
                ? string.Format(eventData.Message, eventData.Payload.ToArray())
                : eventData.Payload[0].ToString();

            var strings = fullMessage.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            var firstRow = strings.FirstOrDefault(str => !string.IsNullOrWhiteSpace(str));

            return firstRow ?? string.Empty;
        }

        private static bool IsErrorOrCritical(EventInformations eventData)
        {
            return eventData.Level == EventLevel.Error || eventData.Level == EventLevel.Critical;
        }

        public bool HasErrors { get; private set; }

        public List<EventInformations> GetLog()
        {
            return new List<EventInformations>(_events);
        }

        public List<EventInformations> GetLogError()
        {
            lock (_eventsLock)
            {
                return _events.Where(IsErrorOrCritical).ToList();
            }
        }

        public IObservable<EventInformations> EventAdded => _eventAdded;
    }
}