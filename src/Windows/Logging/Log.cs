﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using MvvmCross.Base;
using Submissions.Logging;

namespace Submissions.Common.Windows.Logging
{
    public class Log : ILog, IDisposable
    {
        private const string LogFileName = "Log";

        private readonly LogEventSource _eventSource;

        private readonly List<EventListener> _listener = new List<EventListener>();

        private Log()
        {
            _eventSource = new LogEventSource();
            RegisterListener(new LogEventListener(LogFileName), EventLevel.LogAlways);
        }

        public void RegisterListener(EventListener listener, EventLevel levelToListen)
        {
            listener.EnableEvents(_eventSource, levelToListen);
            _listener.Add(listener);
        }

        public static Log Instance { get; } = new Log();

        public bool IsDebugEnabled { get; set; }

        public void Debug(string message)
        {
            if (IsDebugEnabled)
            {
                _eventSource.Debug(message);
            }
        }

        public void Debug(Exception exception)
        {
            if (IsDebugEnabled)
            {
                _eventSource.Debug(exception.Message);
            }
        }

        public void Info(string message)
        {
            _eventSource.Info(message);
        }

        public void Warn(string message)
        {
            _eventSource.Warn(message);
        }

        public void Error(string message)
        {
            _eventSource.Error(message);
        }

        public void Error(Exception exception)
        {
            _eventSource.Error(exception.Message, exception.ToString());
        }

        public void Error(string message, Exception exception)
        {
            _eventSource.Error(message, exception.ToString());
        }

        public void Critical(string message)
        {
            _eventSource.Critical(message);
        }

        public void Dispose()
        {
            foreach (var logEventListener in _listener)
            {
                logEventListener.DisposeIfDisposable();
            }

            _listener.Clear();
        }
    }
}
