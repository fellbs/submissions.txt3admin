﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;
using Submissions.Schedul;

namespace Submissions.Common.Windows.Logging
{
    public class LogEventListener : EventListener
    {
        private const string LogFolderName = "Logs";
        private readonly SemaphoreSlim _lock;
        private readonly string _name;
        private StorageFile _logFile;

        public LogEventListener(string logName)
        {
            _name = logName;
            _lock = new SemaphoreSlim(1);
            AssignLogFile();
        }

        private async void AssignLogFile()
        {
            var logNameWithoutWhitespace = _name.Replace(" ", "_");
            var currentDate = DateTime.Today.ToString("yyyy-MM-dd");
            var fileName = $"{logNameWithoutWhitespace}-{currentDate}.log";
            var logsFolder = await GetLogFolderAsync().ConfigureAwait(true);
            _logFile = await logsFolder.CreateFileAsync(fileName, CreationCollisionOption.OpenIfExists);
        }

        private static async Task<StorageFolder> GetLogFolderAsync()
        {
            var localFolder = ApplicationData.Current.LocalFolder;
            if (await localFolder.TryGetItemAsync(LogFolderName) is StorageFolder logsFolder)
            {
                return logsFolder;
            }

            return await localFolder.CreateFolderAsync(LogFolderName);
        }

        protected override void OnEventWritten(EventWrittenEventArgs eventData)
        {
            if (_logFile == null)
            {
                return;
            }

            var logMessage = eventData.Payload[0];
            if (eventData.Payload.Count == 2 && eventData.Payload[1] is string exceptionMessage)
            {
                if (!string.IsNullOrEmpty(exceptionMessage))
                {
                    logMessage += Environment.NewLine + exceptionMessage;
                }
            }

            var logEntry = $"{DateTime.Now}\t\t{eventData.Level}: {logMessage}";
            Observable.FromAsync(_ => WriteLogEntryToFileAsync(logEntry)).Subscribe();
        }

        private async Task WriteLogEntryToFileAsync(string logEntry)
        {
            await Task.Run(() => WriteLinesToFileAsync(new[] { logEntry })).ContinueOnCurrentContext();
        }

        private async Task WriteLinesToFileAsync(IEnumerable<string> lines)
        {
            try
            {
                await _lock.WaitAsync().ContinueOnCurrentContext();
                await FileIO.AppendLinesAsync(_logFile, lines);
            }
            catch (Exception)
            {
                // do nothing
            }
            finally
            {
                _lock.Release();
            }
        }
    }
}